/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "core_task.hpp"
#include "core.hpp"  // IWYU pragma: keep

// ----------

namespace lvd {

template <class T>
void          Task<T>::exec_state() {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wold-style-cast"
  T* self = (T*) this;
#pragma clang diagnostic pop

  while (!states_.empty()) {
    auto state =    pull_state();
    if (!state) {
      break;
    }

    bool awake = (self->*state)();
    if (!awake) {
      break;
    }
  }
}

template <class T>
auto Task<T>::head_state() -> State {
  if (states_.empty()) {
    return nullptr;
  }

  return states_.top();
}

template <class T>
void Task<T>::push_state(State state) {
  states_.push(state);
}

template <class T>
auto Task<T>::pull_state() -> State {
  if (states_.empty()) {
    return nullptr;
  }

  LVD_FINALLY {
    states_.pop();
  };

  return states_.top();
}

template <class T>
template <typename... StatesT>
bool Task<T>::drop_state (StatesT... states) {
  if (states_.empty()) {
    return false;
  }

  auto constexpr states_size = sizeof...(states);
  if constexpr (states_size > 0) {
    const State& top_state = states_.top();
    for (State  cur_state : { states... }) {
      if (cur_state == top_state) {
        states_.pop();
        return true;
      }
    }

    return false;
  }

  states_.pop();
  return true;
}

template <class T>
template <typename... StatesT>
bool Task<T>::drop_states(StatesT... states) {
  if (states_.empty()) {
    return false;
  }

  bool result = false;

  while (drop_state(states...)) {
    result = true;
  }

  return result;
}

template <class T>
auto Task<T>::size_states() const -> size_t {
  return states_.size();
}

template <class T>
bool Task<T>::empty_states() const {
  return states_.empty();
}

template <class T>
void Task<T>::clear_states() {
  states_ = States();
}

}  // namespace lvd
