/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "metatype.hpp"
#include "core.hpp"  // IWYU pragma: keep

// ----------

namespace lvd {

Metatype::Declarations& Metatype::declarations() {
  static Declarations declarations;
  return declarations;
}

void Metatype::metatype() {
  const Declarations& declarations = Metatype::declarations();

  for (const Declaration& declaration : declarations) {
    declaration();
  }
}

}  // namespace lvd
