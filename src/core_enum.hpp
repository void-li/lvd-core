/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include "core.hpp"
#include "core_eval.hpp"

#include <array>
#include <utility>

// ----------

namespace lvd {
namespace __impl__ {
namespace __enum__ {

[[maybe_unused]]
constexpr size_t size_i(const char* str) {
  size_t size = 0;

  if (str[0] != '\0') {
    size++;
  }

  for (size_t i = 0; str[i] != '\0'; i++) {
    if (str[i] == ',') {
      size++;
    }
  }

  return size;
}

[[maybe_unused]]
constexpr size_t size_j(const char* str) {
  size_t size = 0, stmp = 0;
  bool ekey = true;

  for (size_t i = 0; str[i] != '\0'; i++) {
    if (ekey) {
      switch (str[i]) {
        case ' ':
          break;

        case '=':
        case ',':
          if (stmp > size) {
            size = stmp;
          }

          ekey = str[i] != '=';
          stmp = 0x00;
          break;

        default:
          stmp++;
          break;
      }
    } else {
      switch (str[i]) {
        case ',':
          ekey = true;
          stmp = 0x00;
          break;
      }
    }
  }

  return size;
}

constexpr auto build_item(const char* str, size_t i) {
  for (size_t j = 0; j <= i; j++) {
    if (str[j] == ' ' || str[j] == '=' || str[j] == '\0') {
      return '\0';
    }
  }

  return str[i];
}

template <size_t N, size_t... NS>
constexpr auto build_impl(const char* str, std::index_sequence<NS...>) {
  return std::array<char, N + 1>{{build_item(str, NS)...}};
}

template <size_t N>
constexpr auto build     (const char* str) {
  return build_impl<N>(str, std::make_index_sequence<N>{});
}

template <typename T>
struct strip {
  constexpr explicit strip(T t) : t_(t) {}
  constexpr operator T() const { return t_; }

  template <typename U>
  constexpr const strip& operator=([[maybe_unused]] U u) {
    return *this;
  }

 private:
  T t_;
};

}  // namespace __enum__
}  // namespace __impl__

// ----------

#define LVD_ENUM_BUILD(S) (lvd::__impl__::__enum__::build<size_j>(LVD_STR(S)))
#define LVD_ENUM_STRIP(S) (lvd::__impl__::__enum__::strip<__enum__>)      S

#define LVD_ENUM(Name, ...) \
  LVD_ENUM_IMPL(Name, int, __VA_ARGS__)

#define LVD_ENUM_IMPL(Name, Type, ...)                                         \
  struct Name {                                                                \
    using __bool__ = bool;                                                     \
    using __char__ = char;                                                     \
                                                                               \
    using __type__ = Type;                                                     \
    enum  __enum__ : Type { __VA_ARGS__ };                                     \
                                                                               \
    constexpr Name()                                                           \
        : __enum_valid__(false) {}                                             \
                                                                               \
    constexpr Name(__enum__ value)                                             \
        : __enum_valid__(true ),                                               \
          __enum_value__(         value ) {}                                   \
                                                                               \
    explicit                                                                   \
    constexpr Name(__type__ value)                                             \
        : __enum_valid__(true ),                                               \
          __enum_value__(__enum__(value)) {}                                   \
                                                                               \
    constexpr operator __enum__()       const {                                \
      return value();                                                          \
    }                                                                          \
                                                                               \
    constexpr          __bool__ valid() const {                                \
      return __enum_valid__;                                                   \
    }                                                                          \
                                                                               \
    constexpr          __enum__ value() const {                                \
      return __enum_value__;                                                   \
    }                                                                          \
                                                                               \
    static constexpr const char* To_String(__enum__ val) {                     \
      size_t index = ~0u;                                                      \
      for (size_t i = 0; i < __impl__::size_i; i ++) {                         \
        if (__impl__::vals[i] == val) {                                        \
          index = i;                                                           \
          break;                                                               \
        }                                                                      \
      }                                                                        \
                                                                               \
      if (index != ~0u) {                                                      \
        return &__impl__::keys[index][0];                                      \
      } else {                                                                 \
        LVD_THROW_RUNTIME("no key for val: " + std::to_string(val));           \
      }                                                                        \
    }                                                                          \
                                                                               \
    /****/ constexpr const char* to_string() const {                           \
      return To_String(__enum_value__);                                        \
    }                                                                          \
                                                                               \
    static constexpr Name From_String(const char* str) {                       \
      size_t index = ~0u;                                                      \
      for (size_t i = 0; i < __impl__::size_i; i ++) {                         \
        bool equal = true;                                                     \
                                                                               \
        for (size_t j = 0; j < __impl__::size_j; j ++) {                       \
          if (__impl__::keys[i][j] != str[j] || str[j] == '\0') {              \
            if (__impl__::keys[i][j] != str[j]) {                              \
              equal = false;                                                   \
            }                                                                  \
                                                                               \
            break;                                                             \
          }                                                                    \
        }                                                                      \
                                                                               \
        if (equal) {                                                           \
          index = i;                                                           \
          break;                                                               \
        }                                                                      \
      }                                                                        \
                                                                               \
      if (index != ~0u) {                                                      \
        return Name(__impl__::vals[index]);                                    \
      } else {                                                                 \
        LVD_THROW_RUNTIME("no val for key: " + std::   string(str));           \
      }                                                                        \
    }                                                                          \
                                                                               \
    static constexpr auto __keys_begin__() {                                   \
      return std::begin(__impl__::keys);                                       \
    }                                                                          \
    static constexpr auto __keys_end__  () {                                   \
      return std::end  (__impl__::keys);                                       \
    }                                                                          \
                                                                               \
    static constexpr auto __vals_begin__() {                                   \
      return std::begin(__impl__::vals);                                       \
    }                                                                          \
    static constexpr auto __vals_end__  () {                                   \
      return std::end  (__impl__::vals);                                       \
    }                                                                          \
                                                                               \
    static constexpr auto __size__      () {                                   \
      return __impl__::vals.size();                                            \
    }                                                                          \
                                                                               \
   private:                                                                    \
    _Pragma("clang diagnostic push")                                           \
    _Pragma("clang diagnostic ignored \"-Wpadded\"")                           \
    __bool__ __enum_valid__ = static_cast<__bool__>(0);                        \
    __enum__ __enum_value__ = static_cast<__enum__>(0);                        \
    _Pragma("clang diagnostic pop")                                            \
                                                                               \
    struct __impl__ {                                                          \
      static constexpr const size_t size_i =                                   \
          lvd::__impl__::__enum__::size_i(LVD_STR(__VA_ARGS__));               \
      static constexpr const size_t size_j =                                   \
          lvd::__impl__::__enum__::size_j(LVD_STR(__VA_ARGS__));               \
                                                                               \
      _Pragma("clang diagnostic push")                                         \
      _Pragma("clang diagnostic ignored \"-Wold-style-cast\"")                 \
      static constexpr const std::array<__enum__, size_i  > vals         = {{  \
          LVD_EVAL(LVD_ENUM_STRIP, __VA_ARGS__) }};                            \
      static constexpr const std::array<__char__, size_j+1> keys[size_i] = {   \
          LVD_EVAL(LVD_ENUM_BUILD, __VA_ARGS__)  };                            \
      _Pragma("clang diagnostic pop")                                          \
    };                                                                         \
  }

}  // namespace lvd
