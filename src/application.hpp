/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#if   defined(QT_GUI_LIB)
#include                     <QGuiApplication>
#define LVD_APPLICATION_BASE  QGuiApplication
#elif defined(QT_WIDGETS_LIB)
#include                        <QApplication>
#define LVD_APPLICATION_BASE     QApplication
#else
#include                    <QCoreApplication>
#define LVD_APPLICATION_BASE QCoreApplication
#endif

#include <QEvent>
#include <QObject>
#include <QString>

#include "logger.hpp"
#include "signal.hpp"

// ----------

namespace lvd {

class Application : public LVD_APPLICATION_BASE {
  Q_OBJECT LVD_LOGGER

 public:
  Application(int& argc, char** argv);
  ~Application();

  bool notify(QObject* receiver, QEvent* event) override;

 public:
  static void print();

 signals:
  void failure(const QString& message);

 private:
  Signal* sigintx_ = nullptr;
  Signal* sigterm_ = nullptr;
};

}  // namespace lvd
