/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QCoreApplication>
#include <QDir>
#include <QObject>
#include <QSettings>
#include <QStandardPaths>
#include <QString>

#include "config.hpp"
#include "settings.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class SettingsTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void settings_oap() {
    QString path = QString("%1/%2/%3.conf")
        .arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
        , "Org.Name", "App.Name");

    Settings settings(                                org_name_, app_name_);
    QCOMPARE(settings.fileName(), path);
  }

  void settings_soap() {
    QFETCH(QSettings::Scope, scope);

    QString path;

    if (scope == QSettings::UserScope) {
      path = QString("%1/%2/%3.conf")
          .arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
          , "Org.Name", "App.Name");
    } else {
      path = QString("%1/%2/%3.conf")
          .arg(config::SystemPath_Etc()
          , "Org.Name", "App.Name");
    }

    Settings settings(                         scope, org_name_, app_name_);
    QCOMPARE(settings.fileName(), path);
  }

  void settings_soap_data() {
    QTest::addColumn<QSettings::Scope>("scope");

    QTest::addRow("config") << QSettings::  UserScope;
    QTest::addRow("system") << QSettings::SystemScope;
  }

  void settings_fsoap() {
    QFETCH(QSettings::Scope, scope);

    QString path;

    if (scope == QSettings::UserScope) {
      path = QString("%1/%2/%3.conf")
          .arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
          , "Org.Name", "App.Name");
    } else {
      path = QString("%1/%2/%3.conf")
          .arg(config::SystemPath_Etc()
          , "Org.Name", "App.Name");
    }

    Settings settings(QSettings::NativeFormat, scope, org_name_, app_name_);
    QCOMPARE(settings.fileName(), path);
  }

  void settings_fsoap_data() {
    QTest::addColumn<QSettings::Scope>("scope");

    QTest::addRow("config") << QSettings::  UserScope;
    QTest::addRow("system") << QSettings::SystemScope;
  }

  void settings_ffp() {
    QString path = QString("%1/%2.%3.conf")
        .arg(QDir::currentPath()
        , "name.org", "app.name");

    Settings settings("name.org.app.name.conf", QSettings::NativeFormat);
    QCOMPARE(settings.fileName(), path);
  }

  void settings_sp() {
    QFETCH(QSettings::Scope, scope);

    QString path;

    if (scope == QSettings::UserScope) {
      path = QString("%1/%2/%3.conf")
          .arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
          , "addr.org", "app.name");
    } else {
      path = QString("%1/%2/%3.conf")
          .arg(config::SystemPath_Etc()
          , "addr.org", "app.name");
    }

#if QT_VERSION >= QT_VERSION_CHECK(5, 13, 0)
    Settings settings(                         scope                      );
    QCOMPARE(settings.fileName(), path);
#else
    Q_UNUSED(scope)
    Q_UNUSED(path)

    QSKIP("requires Qt >= 5.13");
#endif
  }

  void settings_sp_data() {
    QTest::addColumn<QSettings::Scope>("scope");

    QTest::addRow("config") << QSettings::  UserScope;
    QTest::addRow("system") << QSettings::SystemScope;
  }

  void settings_p() {
    QString path = QString("%1/%2/%3.conf")
        .arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
        , "addr.org", "app.name");

    Settings settings;
    QCOMPARE(settings.fileName(), path);
  }

  // ----------

 private slots:
  void init() {
    Test::init();

    QCoreApplication::setApplicationName (app_name_);
    QCoreApplication::setApplicationVersion(app_vers_);

    QCoreApplication::setOrganizationName(org_name_);
    QCoreApplication::setOrganizationDomain(org_addr_);
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };

    QCOMPARE(QCoreApplication::applicationName (), app_name_);
    QCOMPARE(QCoreApplication::applicationVersion(), app_vers_);

    QCOMPARE(QCoreApplication::organizationName(), org_name_);
    QCOMPARE(QCoreApplication::organizationDomain(), org_addr_);
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }

 private:
  QString app_name_ = "App.Name";
  QString app_vers_ = "App.Vers";

  QString org_name_ = "Org.Name";
  QString org_addr_ = "Org.Addr";
};

LVD_TEST_MAIN(SettingsTest)
#include "settings_test.moc"  // IWYU pragma: keep
